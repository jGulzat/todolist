//
//  TaskViewModel.swift
//  AlamofireMVVm
//
//  Created by Guzlat on 16/1/22.
//

import Foundation
import Alamofire

class TaskViewModel{
    private let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MWUzZjQzYTA1NjRhMjAwMTdjOTkzZDkiLCJpYXQiOjE2NDIzMjkxNDZ9.FbkugRia9EzUaACsat_nTkRy48Yey4vl5rE4DCuvacQ"
    private let baseURL = "https://api-nodejs-todolist.herokuapp.com/task"
    
    //var listCount: Int = 5
    var taskName = "text"
    private var _taskList: [String] = []
    private var _filteredTaskList: [String] = []
    private var _isFiltering: Bool = false
    private var _taskInfoList: [TaskInfo] = []
    
    func filterTaskList(_ searchText: String, _ completion: @escaping () -> () ){
        _isFiltering = true
        _filteredTaskList = _taskList.filter { $0.lowercased().contains(searchText.lowercased()) }
        
        completion()
    }
    
    func getTaskList(_ completion: @escaping () -> () ){
        let headers: HTTPHeaders =
        ["Authorization": "Bearer \(token)", "Content-Type": "application/json"]
        
        guard let url = URL(string: baseURL) else {
            print("url not invalid")
            return
        }
        AF.request(url, method: .get, headers: headers).response { response in
            if let error = response.error{
                print("some Error: \(String(describing: error))")
                return
            }
            guard let data = response.data else { return }
            
            do {
                let tasks = try JSONDecoder().decode(TaskModel.self, from: data)
                self._taskInfoList = tasks.data
                tasks.data.forEach { task in
                    self._taskList.append(task.description)
                }
            } catch let jsonError {
                print("error in getting list: \(jsonError)")
            }
            completion()
        }
    }
    
    func addTask(_ task: String, _ completion: @escaping () -> ()){
        let headers: HTTPHeaders =
        ["Authorization": "Bearer \(token)", "Content-Type": "application/json"]
        
        guard let url = URL(string: baseURL) else {
            print("url not invalid")
            return
        }
        let param = ["description": task]

        AF.request(url, method: .post,
                   parameters: param,
                   encoding: JSONEncoding.prettyPrinted,
                   headers: headers).response { response in
            if let error = response.error {
                print("some Error: \(String(describing: error))")
                return
            }
            guard let data = response.data else { return }
            
            do {
                let tasks = try JSONDecoder().decode(ResultAdd.self, from: data)
                print("tasks: \(tasks.data.description)")
                self._taskList.append(tasks.data.description)
                self._taskInfoList.append(tasks.data)
            } catch let jsonError {
                print("error in getting list: \(jsonError)")
            }
            completion()
        }
    }
    
    func deleteTask(_ index: Int, _ completion: @escaping () -> ()){
        let headers: HTTPHeaders =
        ["Authorization": "Bearer \(token)", "Content-Type": "application/json"]
        let taskId = _taskInfoList[index]._id
        guard let url = URL(string: baseURL + "/" + taskId) else {
            print("url not invalid")
            return
        }
        
        AF.request(url, method: .delete, headers: headers).response{ response in
            if let error = response.error {
                print("some Error: \(String(describing: error))")
                return
            }
            guard let data = response.data else { return }
            
            do {
                let resultDelete = try JSONDecoder().decode(ResultDelete.self, from: data)
                if resultDelete.success{
                    completion()
                    self._taskList.remove(at: index)
                    self._taskInfoList.remove(at: index)
                }
            } catch let jsonError {
                print("error in getting list: \(jsonError)")
            }
        }
    }
    
    func updateTask(_ index: Int, _ newValue: String, _ completion: @escaping () -> ()){
        let headers: HTTPHeaders =
        ["Authorization": "Bearer \(token)", "Content-Type": "application/json"]
        let taskId = _taskInfoList[index]._id
        guard let url = URL(string: baseURL + "/" + taskId) else {
            print("url not invalid")
            return
        }
        let param = ["description": newValue]
        AF.request(url, method: .put, parameters: param,
                   encoding: JSONEncoding.prettyPrinted,
                   headers: headers).response {response in
            if let error = response.error {
                print("some Error: \(String(describing: error))")
                return
            }
            guard let data = response.data else { return }
            
            do {
                let resultUpdate = try JSONDecoder().decode(ResultAdd.self, from: data)
                if resultUpdate.success {
                  
                   self._taskInfoList[index].description = newValue
                    self._taskList[index] = newValue
                    completion()
                }
            } catch let jsonError {
                print("error in getting list: \(jsonError)")
            }
        }
    }
        
    var numberOfRowsInSection: Int {
        get { _isFiltering ? _filteredTaskList.count : _taskList.count }
    }
    
    func getTask(_ at: Int) -> String? {
        numberOfRowsInSection > at
        ? _isFiltering ? _filteredTaskList[at] : _taskList[at]
        : nil
    }
    
    var isFiltering: Bool{
        get { _isFiltering }
    }
    func filterStop(){
        _isFiltering = false
    }
}
