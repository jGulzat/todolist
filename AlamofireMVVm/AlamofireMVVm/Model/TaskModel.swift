//
//  TaskModel.swift
//  AlamofireMVVm
//
//  Created by Guzlat on 16/1/22.
//

import Foundation

struct ResultAdd: Decodable{
    var success: Bool
    var data: TaskInfo
}

struct TaskModel: Decodable {
    var count: Int
    var data: [TaskInfo]
}

struct TaskInfo: Decodable {
    var completed : Bool
    var _id : String
    var description: String
    var owner: String
   //var createdAt: Date,
   // var updatedAt: Date,
   // var __v: 0
}

struct ResultDelete: Decodable{
    var success: Bool
}
