//
//  SettingVC.swift
//  AlamofireMVVm
//
//  Created by Guzlat on 2/2/22.
//

import UIKit
import SnapKit
import RadioGroup

enum Size: CGFloat {
    case large = 30.0
    case medium = 18.0
    case litte = 11.0
}

enum Style: String {
    case bold = "Montserrat-Bold"
    case italic = "Montserrat-Italic"
    case thin = "Montserrat-Thin"
}

class SettingVC: UIViewController {
    
    let userDefaults = UserDefaults.standard

    private lazy var sizeBtn: UIButton = {
        let sizeBtn = UIButton()
        sizeBtn.setTitleColor(.black, for: .normal)
        sizeBtn.setTitle("Размер шрифта", for: .normal)
        sizeBtn.layer.cornerRadius = 8.0
        
        return sizeBtn
    }()
    
    private lazy var styleBtn: UIButton = {
        let style = UIButton()
        style.setTitleColor(.black, for: .normal)
        style.setTitle("Стиль шрифта", for: .normal)
        style.layer.cornerRadius = 8.0
    
        return style
    }()
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            view.backgroundColor = .white

            sizeBtn.addTarget(self, action: #selector(changeSize), for: .touchUpInside)
            view.addSubview(sizeBtn)
            sizeBtn.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(250.0)
                make.centerX.equalToSuperview()
                make.height.equalTo(44.0)
            }
            
            styleBtn.addTarget(self, action: #selector(changeStyle), for: .touchUpInside)
            view.addSubview(styleBtn)
            styleBtn.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalTo(sizeBtn.snp.bottom).offset(24.0)
                make.height.equalTo(44.0)
            }
    }
    // alert for change size
    @objc func changeSize(_ sender: UIButton){
        let radioGroup = RadioGroup(titles: ["Large", "Medium", "Little"])
        let sizeAlert = UIAlertController(title: "Размер шрифта", message: "", preferredStyle: .alert)
        let selectedIndex = userDefaults.integer(forKey: "sizeNumber") 
        
        RadioGroup.appearance().titleColor = .black
        radioGroup.buttonSize = 24.0
        radioGroup.spacing = 12.0
        radioGroup.itemSpacing = 100.0
        radioGroup.selectedIndex = selectedIndex
        
        radioGroup.titleAlignment = .left
        radioGroup.isButtonAfterTitle = true
        
        sizeAlert.view.addSubview(radioGroup)
        
        radioGroup.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(60.0)
            make.leading.trailing.equalToSuperview().inset(24.0)
        }
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: sizeAlert.view as Any,
                                                           attribute: NSLayoutConstraint.Attribute.height,
                                                           relatedBy: NSLayoutConstraint.Relation.equal,
                                                           toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                                           multiplier: 1, constant: self.view.frame.height * 0.30)
        sizeAlert.view.addConstraint(height);
            
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            
            switch radioGroup.selectedIndex {
            case 0: do {
                self.userDefaults.set(Size.large.rawValue, forKey: "size")
                self.userDefaults.set(0, forKey: "sizeNumber")
            }
            case 1: do {
                self.userDefaults.set(Size.medium.rawValue, forKey: "size")
                self.userDefaults.set(1, forKey: "sizeNumber")
            }
            case 2: do {
                self.userDefaults.set(Size.litte.rawValue, forKey: "size")
                self.userDefaults.set(2, forKey: "sizeNumber")
            }
            default: do {
                self.userDefaults.set(Size.medium.rawValue, forKey: "size")
                self.userDefaults.set(1, forKey: "sizeNumber")
            }
          }
        }
        
        sizeAlert.addAction(cancelAction)
        sizeAlert.addAction(okAction)
        present(sizeAlert, animated: true, completion: nil)
  }

    // alert for change style
    @objc func changeStyle(_ sender: UIButton){
        let radioGroup = RadioGroup(titles: ["Bold", "Italic", "Thin"])
        
        let styleAlert = UIAlertController(title: "Стиль шрифта", message: "", preferredStyle: .alert)
        let selectedIndex = userDefaults.integer(forKey: "styleNumber")
        
        RadioGroup.appearance().titleColor = .black
        radioGroup.buttonSize = 24.0
        radioGroup.spacing = 12.0             // spacing between buttons
        radioGroup.itemSpacing = 100.0
        radioGroup.selectedIndex = selectedIndex
        
        radioGroup.titleAlignment = .left
        radioGroup.isButtonAfterTitle = true
        
        styleAlert.view.addSubview(radioGroup)
        
        radioGroup.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(60.0)
            make.leading.trailing.equalToSuperview().inset(24.0)
        }
        
        let height:NSLayoutConstraint = NSLayoutConstraint(item: styleAlert.view as Any,
                                                           attribute: NSLayoutConstraint.Attribute.height,
                                                           relatedBy: NSLayoutConstraint.Relation.equal,
                                                           toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                                                           multiplier: 1, constant: self.view.frame.height * 0.30)
        styleAlert.view.addConstraint(height);
            
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            
            switch radioGroup.selectedIndex {
            case 0: do {
                self.userDefaults.set(Style.bold.rawValue, forKey: "style")
                self.userDefaults.set(0, forKey: "styleNumber")
             }
            case 1: do {
                self.userDefaults.set(Style.italic.rawValue, forKey: "style")
                self.userDefaults.set(1, forKey: "styleNumber")
             }
            case 2: do {
                self.userDefaults.set(Style.thin.rawValue, forKey: "style")
                self.userDefaults.set(2, forKey: "styleNumber")
             }
            default: do {
                self.userDefaults.set(Style.italic.rawValue, forKey: "style")
                self.userDefaults.set(1, forKey: "styleNumber")
             }
            }
        }
        
        styleAlert.addAction(cancelAction)
        styleAlert.addAction(okAction)
        present(styleAlert, animated: true, completion: nil)
   }
}
