//
//  TaskViewController.swift
//  AlamofireMVVm
//
//  Created by Guzlat on 16/1/22.
//

import UIKit

class TaskViewController: UIViewController {

    let userDefaults = UserDefaults.standard

    private var searchController = UISearchController(searchResultsController: nil)
    private let tableView = UITableView()
    var viewModel: TaskViewModel
    
    init(_ viewModel: TaskViewModel){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initUI()
        getTaskList()
        
    }
    private func getTaskList(){
        viewModel.getTaskList(){
            self.tableView.reloadData()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    private func initUI(){
        title = "Заметки"
        view.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1)
        view.addSubview(tableView)
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        navigationController?.navigationBar.prefersLargeTitles = true
        searchController.obscuresBackgroundDuringPresentation = false
        let settingBtn = UIBarButtonItem(title: "Setting",
                                         style: .plain, target: self,
                                         action: #selector(setting))
        self.navigationItem.leftBarButtonItem  = settingBtn
        
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.frame = view.bounds
        tableView.delegate = self
        tableView.dataSource = self
        navigationItem.rightBarButtonItem = .init(barButtonSystemItem: .add, target: self, action: #selector(addNewTask))
    }
    
    @objc func addNewTask(sender: Any){
        let addTaskAlert = UIAlertController(title: "New task",
                                             message: "Enter a new task",
                                             preferredStyle: .alert)
        addTaskAlert.addTextField { $0.placeholder = "Task name" }
        addTaskAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
            guard let self = self,
                  let nameTF = addTaskAlert.textFields?.first,
                  let newToDo = nameTF.text else { return }
            
            self.viewModel.addTask(newToDo){
                self.tableView.reloadData()
            }
        }))
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        addTaskAlert.addAction(cancelAction)
        present(addTaskAlert, animated: true, completion: nil)

    }
    @objc func setting(_ sender: UIBarButtonItem){
        let setting = SettingVC()
        self.navigationController?.pushViewController(setting, animated: true)
    }
}

extension TaskViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let size = userDefaults.float(forKey: "size")
        let style = userDefaults.string(forKey: "style")
        cell.textLabel?.text = viewModel.getTask(indexPath.row)
        cell.textLabel?.font = UIFont(name: style ?? "Montserrat-Italic", size: CGFloat(size))
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView,
                   commit editingStyle: UITableViewCell.EditingStyle,
                   forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete: deleteTask(indexPath)
        case .insert, .none: break
        @unknown default: break
        }
    }
    func deleteTask(_ indexPath: IndexPath){
        
        self.viewModel.deleteTask(indexPath.row) {
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchController.isActive = false
        
        guard let oldTask = self.viewModel.getTask(indexPath.row) else{ return }
        let editAlert = UIAlertController(title: "Edit task",
                                      message: "Selected: \(oldTask)",
                                      preferredStyle: .alert)
        editAlert.addTextField { $0.text = oldTask }
        
        let action = UIAlertAction(title: "Ok", style: .default) { alertAction in
            guard let nameTF = editAlert.textFields?.first,
                  let newTask = nameTF.text else { return }
            if newTask.count > 0 {
                self.viewModel.updateTask(indexPath.row, newTask) {
                    self.tableView.reloadData()
                }
            }
           
        }
       editAlert.addAction(action)
       self.present(editAlert, animated: true, completion: nil)
    }
}
extension TaskViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel.filterTaskList(searchText) {
            self.tableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.viewModel.filterStop()
        self.tableView.reloadData()
    }
}
